<?php

namespace Drupal\routing\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase {
    
    protected function alterRoutes(RouteCollection $collection) {
        if ($route = $collection->get('user.login')) {
            $route->setPath('/giris');
        }
        if ($route = $collection->get('user.logout')) {
            $route->setPath('/cikis');
        }
    }
}